/* eslint-disable no-undef */
/* eslint no-console: 0 */

const TWAddress = require('../index');

jest.setTimeout(1000 * 60 * 10);

test('Test TWAddress', async () => {
  console.log(TWAddress.APIURL);
  console.log(TWAddress.RequestError);
  console.log(TWAddress.InvalidCountyError);

  console.log('Current year of ROC', TWAddress.getROCYear(TWAddress.getYear()));

  const lister = new TWAddress(111);
  // test error handle
  try {
    await lister.getRoadList();
  } catch (err) {
    console.error(err);
  }
  // test get all roads
  const counties = TWAddress.getCountyList();
  for (const county of counties) {
    const towns = TWAddress.getTownList(county);
    for (const town of towns) {
      await lister.getRoadList(county, town);
      // console.log(county, town);
    }
  }
});
