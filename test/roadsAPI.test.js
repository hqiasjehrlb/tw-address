/* eslint-disable no-undef */
/* eslint no-console: 0 */

const roadsAPI = require('../lib/roadsAPI');

test('Test roadsAPI', async () => {
  console.log(new roadsAPI.RequestError(400, 'TEST', 'TEST'));

  try {
    await roadsAPI.APICall('https://www.ris.gov.tw/rs-opendata/test', 'TEST');
  } catch (err) {
    console.error(err);
  }

  await roadsAPI.APICall('https://www.ris.gov.tw/rs-opendata/api/v1/datastore/ODRP049', 111);
});
