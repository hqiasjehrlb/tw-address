/* eslint-disable no-undef */
/* eslint no-console: 0 */

const districts = require('../lib/districts');

test('Test districts', () => {
  console.log(new districts.InvalidCountyError('TEST'));

  try {
    districts.getTownList();
  } catch (err) {
    console.error(err);
  }

  const [county] = districts.getCountyList();
  districts.getTownList(county);
});
